'''
 Removes white background from png file. 
 Usage:
 python make_transparent.py file.png
 produces file_tr.png

'''
from PIL import Image
import sys

try:
    sys.argv[1]
except IndexError:
    print('Lack of file name, syntax: python make_transparent.py file.png')

print('File to be opened: {}'.format(str(sys.argv[1])))
pngfile=sys.argv[1]

img = Image.open(pngfile)

img = img.convert("RGBA")
datas = img.getdata()

newData = []
for item in datas:
    if item[0] == 255 and item[1] == 255 and item[2] == 255:
        newData.append((255, 255, 255, 0))
    else:
        newData.append(item)

img.putdata(newData)
outputpng=pngfile[:-4]+"_tr.png"
print('Output file: {}'.format(str(outputpng)))
img.save(outputpng, "PNG")
