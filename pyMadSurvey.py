'''
    Methods allowing to plot madx survey fike using matplotlib patches.
    Work supported by DLR contract
    Mariusz Sapinski and Mirsad Tunja, 2020.02.10
    - developed and tested with python 3.7.6 and 3.8.2
    - last version: 2020.08.14

'''

from matplotlib import pyplot as plt
from matplotlib import transforms
import matplotlib.patches as patches
from math import sqrt


class MadSurvey:
    '''
    The class containing methods to read and display MADX survey files.
    '''

    def __init__(self, name=''):
        self.name=''       # for the surveys composed of many segments
        self.xelend = []   # end position of the element (as in survey file)
        self.yelend = []   # end position of the element (as in survey file)
        self.zelend = []   # --||--
        self.anglel = []   # the element rotation angle
        self.lengel = []   # length of the element
        self.typeel = []   # type of the element
        self.nameel = []   # name of the element
        self.sposel = []   # s-position of an element
        self.dangle = []   # dipole angle
        self.xelect  = []   # middle position of an element (derived)
        self.yelect  = []   # middle position of an element (derived)
        self.zelect  = []   # --||--
        self.xelest  = []   # starting position of an element (derived)
        self.yelest  = []   # starting position of an element (derived)
        self.zelest  = []   # --||--
        self.cleng  = []    # lengths calculated from start and end points

    def loadSurvey(self, filename):
        '''
        Loads MADX survey file to internal arrays/lists.
        '''
        for line in open(filename,'r'):
            if line[0]=='@' or line[0]=='$':     # skip header lines
                pass
            elif line[0]=='*':   # column definitions
                coldef=line.split()
                print(coldef)
                print(coldef.index('X'))
            else:
                eline=line.split()
                #print('loadSurvey, eline:',eline)
                self.xelend.append(float(eline[coldef.index('X')-1]))  # end-point of an element (is it?)
                self.yelend.append(float(eline[coldef.index('Y')-1]))  # end-point of an element
                self.zelend.append(float(eline[coldef.index('Z')-1]))  # end-point of an element
                self.anglel.append(float(eline[coldef.index('THETA')-1]))
                self.lengel.append(float(eline[coldef.index('L')-1]))
                self.typeel.append(eline[coldef.index('KEYWORD')-1])
                self.dangle.append(float(eline[coldef.index('ANGLE')-1])) # dipole angle                
                #print(eline[coldef.index('KEYWORD')-1])
                self.nameel.append(eline[coldef.index('NAME')-1])
                #print('loadSurvey:',self.xelend[-1],self.yelend[-1],self.zelend[-1])
        #print(self.xelend)
        self.calcCentres()  # to calculate centers of the elements
        self.calcLength()   # to calculate lengths

    def compareLengths(self):
        '''
        Compares lengths of the elements with real lengths.
        '''
        for idex,x in enumerate(self.cleng):
            print('element lengths = ',self.nameel[idex],self.typeel[idex],x,self.lengel[idex])

        
        
    def plotVacEnd(self):
        '''
        Plots vacuum chamber based on end points of the elements.
        '''
        plt.plot(self.xelend,self.zelend)
        #plt.show()

    def calcCentres(self):
        '''
        This is a comment, write docstring.
        Calculates beginnings and centers of the elements based on end point of the previous element.
	    But there seems to be a problem with this approach.
	    Maybe we need to do it based on end points, lengths and angles.
        '''
        for idex,x in enumerate(self.xelend):
            y=self.yelend[idex]
            z=self.zelend[idex]
            theta=self.anglel[idex]
            l=self.lengel[idex]
            if idex==0:  # first element, special case, it should be OK check this!
                self.xelect.append(x)
                self.yelect.append(y)
                self.zelect.append(z)
                self.xelest.append(x)
                self.yelest.append(y)
                self.zelest.append(z)
            else:
                deltax=x-self.xelend[idex-1]
                deltay=y-self.yelend[idex-1]
                deltaz=z-self.zelend[idex-1]
                self.xelect.append(x-deltax/2)
                self.yelect.append(y-deltay/2)
                self.zelect.append(z-deltaz/2)
                self.xelest.append(self.xelend[idex-1])
                self.yelest.append(self.yelend[idex-1])
                self.zelest.append(self.zelend[idex-1])
            #print("calcCentres x: ",self.nameel[idex],self.xelend[idex],l,self.xelest[idex])
            #print("calcCentres z:                   ",self.zelend[idex],l,self.zelest[idex])

    def plotElem(self, idex, rangle=0, hshift=0, vshift=0, projection='XZ', linewidth=1, color='black'):
        '''
        Abstract class, plots an element of a given index as a section.
        hshift and vshift refer to shift of element position XZ plane.
        Future: change meaning of hshift, vshift depending on projection.
        '''
        base = plt.gca()
        rotq = transforms.Affine2D().rotate_deg(rangle)+base.transData
        offset = transforms.Affine2D().translate(hshift, vshift)
        rotshift=rotq+offset
        if projection=='XZ':
            print('plotting XZ, linewidth = ',linewidth)
            plt.plot([self.xelend[idex],self.xelest[idex]],[self.zelend[idex],self.zelest[idex]],'-',linewidth=linewidth, color=color, transform=rotshift)
        elif projection=='XY':    
            plt.plot([self.xelend[idex],self.xelest[idex]],[self.yelend[idex],self.yelest[idex]],'-',linewidth=linewidth, color=color, transform=rotshift)
        elif projection=='YZ':    
            plt.plot([self.yelend[idex],self.yelest[idex]],[self.zelend[idex],self.zelest[idex]],'-',linewidth=linewidth, color=color, transform=rotshift)            
        plt.gca().set_aspect('equal', adjustable='box')


    def plotVacMid(self, rangle=0, hshift=0, vshift=0):
        '''
        Plots any element (vacuum chamber) based on middles (centres) of the elements.
        rangle is plot rotation angle in degrees. It goes counterclockwise.

        '''
        base = plt.gca()
        rot = transforms.Affine2D().rotate_deg(rangle)+base.transData
        #shift=(xshift, yshift)
        offset = transforms.Affine2D().translate(hshift, vshift)
        rotshift=rot+offset
        plt.plot(self.xelect, self.zelect, c='black', transform=rotshift)
        plt.gca().set_aspect('equal', adjustable='box')
        #plt.show()

    def plotVac(self, rangle=0, hshift=0, vshift=0, projection='XZ'):
        base = plt.gca()
        rot = transforms.Affine2D().rotate_deg(rangle)+base.transData
        #shift=(xshift, yshift)
        offset = transforms.Affine2D().translate(hshift, vshift)
        rotshift=rot+offset
        if projection=='XZ':
            co1=[(x1, x2, x3) for x1, x2, x3 in zip(self.xelest,self.xelect,self.xelend)]
            co2=[(x1, x2, x3) for x1, x2, x3 in zip(self.zelest,self.zelect,self.zelend)]
        if projection=='XY':
            #co1=[(x1, x2) for x1, x2 in zip(self.xelest,self.xelect)]
            #co2=[(x1, x2) for x1, x2 in zip(self.yelest,self.yelect)]
            co1 = [item for sublist in zip(self.xelest,self.xelect) for item in sublist]
            co2 = [item for sublist in zip(self.yelest,self.yelect) for item in sublist]
        print(co1)
        plt.plot(co1,co2, c='black', transform=rotshift)
        plt.gca().set_aspect('equal', adjustable='box')

    def plotDrift(self, rangle=0, hshift=0, vshift=0, projection='XZ'):
        '''
        Plots drift spaces along the beam line.
        Better then plotVac!
        '''
        base = plt.gca()
        for idex,typq in enumerate(self.typeel):
            if 'DRIFT' in typq:
                self.plotElem(idex, rangle, hshift, vshift, projection,linewidth=1,color='black')
        plt.gca().set_aspect('equal', adjustable='box')
        #plt.plot(xqpos,zqpos,'o')

    def plotVacMidZY(self, rangle=0):
        '''
        Plots vacuum chamber based on middles (centres) of the elements.
        rangle is plot rotation angle in degrees. It goes counterclockwise.

        '''
        base = plt.gca()
        rot = transforms.Affine2D().rotate_deg(rangle)+base.transData
        #plt.plot(self.xelect, self.yelect, c='black', transform=rot)
        plt.plot(self.zelect, self.yelect, c='black')
        plt.gca().set_aspect('equal', adjustable='box')

    # -----------------------------------------------------------------
    def plotQuads(self, rangle=0, hshift=0, vshift=0, projection='XZ', color='red'):
        '''
        Plots quadrupoles along the beam line.
        '''
        for idex,typq in enumerate(self.typeel):
            if 'QUADRUPOLE' in typq:
                if not 'ECR' in self.nameel[idex]:
                    # here maybe is some cheating (--):
                    self.plotElem(idex, rangle, hshift, vshift, projection, linewidth=6, color='red')
                else:  # faking ECRIS as quadrupole, to be removed afterwards
                    self.plotElem(idex, rangle, hshift, vshift, projection, linewidth=10, color='orange')
        plt.gca().set_aspect('equal', adjustable='box')

    # -----------------------------------------------------------------
    def plotRF(self, rangle=0, hshift=0, vshift=0, projection='XZ'):
        '''
        Plots RF elements
        '''
        for idex,typq in enumerate(self.typeel):
            if 'RFCAVITY' in typq:
                self.plotElem(idex, rangle, hshift, vshift, projection, linewidth=6, color='fuchsia')
        plt.gca().set_aspect('equal', adjustable='box')


        
    # -----------------------------------------------------------------
    def plotDipoles(self, rangle=0, hshift=0, vshift=0, projection='XZ', dsplit=True):
        '''
        Plots dipoles along the beam line. Split in 2 halfs (2020Apr27).
        It does not help because centre of the dipole is calculated as if it was a striaght line.
        Split function works for positive bending angle only.
        '''
        base = plt.gca()
        rotq = transforms.Affine2D().rotate_deg(rangle)+base.transData
        offset = transforms.Affine2D().translate(hshift, vshift)
        rotshift=rotq+offset
        print(rotshift)
        xqpos=[]
        zqpos=[]
        for idex,typq in enumerate(self.typeel):
            #print(typq)
            # split in three:
            if 'SBEND' in typq:
                # is this already calculated?: cleng!!!
                Lsh=sqrt(pow(self.xelend[idex]-self.xelest[idex],2)+pow(self.zelend[idex]-self.zelest[idex],2))
                Llo=self.lengel[idex]
                if pow(Llo,2)-pow(Lsh,2)>0:
                    delta=0.5*sqrt(pow(Llo,2)-pow(Lsh,2))
                else:
                    delta=0.0
                print('SBEND',self.nameel[idex],Llo,self.xelend[idex],self.xelest[idex]) # debug
                if self.dangle[idex]<0.0:
                    sidec=+1
                else:
                    sidec=-1
                if self.xelend[idex]-self.xelest[idex]==0.0:
                    xc=self.xelect[idex]+sidec*delta
                    #zc=self.zelect[idex] - this should be, but it gives nothing
                else:
                    xc=self.xelect[idex]-sidec*delta*(self.zelend[idex]-self.zelest[idex])/Lsh
                    #xc=self.xelect[idex]+delta*Lsh/(4*(self.xelend[idex]-self.xelest[idex]))
                if self.zelend[idex]-self.zelest[idex]==0.0:
                    zc=self.zelect[idex]-sidec*delta
                else:
                    zc=self.zelect[idex]+sidec*delta*(self.xelend[idex]-self.xelest[idex])/Lsh
                    #zc=self.zelect[idex]+delta*Lsh/(4*(self.zelend[idex]-self.zelest[idex]))
                print('projection: ',projection)
                if projection=='XZ' and dsplit==True:
                    print('with dsplit')
                    co1=[self.xelend[idex],xc,self.xelest[idex]] # first coordinate
                    co2=[self.zelend[idex],zc,self.zelest[idex]] # second coordinate
                elif projection=='XZ' and dsplit==False:
                    print('no dsplit')
                    co1=[self.xelend[idex],self.xelect[idex],self.xelest[idex]] # first coordinate
                    co2=[self.zelend[idex],self.zelect[idex],self.zelest[idex]] # second coordinate
                elif projection=='XY':
                    co1=[self.xelend[idex],self.xelect[idex],self.xelest[idex]] # first coordinate
                    co2=[self.yelend[idex],self.yelect[idex],self.yelest[idex]] # second coordinate
                # investigate why the first one is not working
                plt.plot(co1,co2,'-',linewidth=11,c='blue', transform=rotshift)
                #plt.plot(co1,co2,'-',linewidth=10,c='blue', transform=rotq)
        plt.gca().set_aspect('equal', adjustable='box')


    # -----------------------------------------------------------------
    def plotKickers(self,rangle, hshift=0, vshift=0, projection='XZ'):
        '''
        Plots kickers (including scanning magnets) along the beam line as green diamonds.
        '''
        base = plt.gca()
        rot = transforms.Affine2D().rotate_deg(rangle)+base.transData
        offset = transforms.Affine2D().translate(hshift, vshift)
        rotshift=rot+offset        
        xqpos=[]
        zqpos=[]
        for idex,typq in enumerate(self.typeel):
            #print(typq)
            # it is either HKICKER or VKICKER
            if 'KICKER' in typq:
                if projection=='XZ':
                    plt.plot([self.xelend[idex]],[self.zelend[idex]],'d',markersize=10,c='green', transform=rotshift)
                elif projection=='XY':
                    plt.plot([self.xelend[idex]],[self.yelend[idex]],'d',markersize=10,c='green', transform=rotshift)
        plt.gca().set_aspect('equal', adjustable='box')


    # -----------------------------------------------------------------
    def plotDipolesSimple(self,rangle,hshift,vshift,projection='XZ'):
        '''
        Plots dipoles along the beam line. From side. Remove, incorporated into plotDipole function
        '''
        for idex,typq in enumerate(self.typeel):
            #print(typq)
            if 'SBEND' in typq:
                self.plotElem(idex, rangle, hshift, vshift, projection, linewidth=10, color='blue')
        plt.gca().set_aspect('equal', adjustable='box')

    # -----------------------------------------------------------------
    def calcLength(self):
        '''
        Calculate length from beginning and end coordinates.
        '''
        for idex,elend in enumerate(self.xelend):
            self.cleng.append(sqrt(pow(elend-self.xelest[idex],2)+pow(self.zelend[idex]-self.zelest[idex],2)))

    # -----------------------------------------------------------------
    def plotPatient(self, rangle, hshift, vshift,  projection='XZ'):
        '''
        Plots location of the patients.
        '''
        base = plt.gca()
        rot = transforms.Affine2D().rotate_deg(rangle)+base.transData
        offset = transforms.Affine2D().translate(hshift, vshift)
        rotshift=rot+offset           
        #xqpos=[] 
        #zqpos=[]
        for idex,namq in enumerate(self.nameel):
            #print(typq)
            if 'PATIENT' in namq:
                print(self.xelend[idex],self.zelend[idex],self.xelest[idex],self.zelest[idex])
                if projection=='XZ':
                    plt.plot([self.xelend[idex],self.xelest[idex]],[self.zelend[idex],self.zelest[idex]],marker='$P$',markersize=13,c='green', transform=rotshift)
                elif projection=='XY':
                    plt.plot([self.xelend[idex],self.xelest[idex]],[self.yelend[idex],self.yelest[idex]],marker='$P$',markersize=13,c='green', transform=rotshift)
        plt.gca().set_aspect('equal', adjustable='box')


    # -----------------------------------------------------------------
    def plotDump(self, rangle, hshift, vshift, projection='XZ'):
        '''
        Plots location of the beam dump.
        '''
        base = plt.gca()
        rot = transforms.Affine2D().rotate_deg(rangle)+base.transData
        offset = transforms.Affine2D().translate(hshift, vshift)
        rotshift=rot+offset           
        #xqpos=[]
        #zqpos=[]
        for idex,namq in enumerate(self.nameel):
            #print(typq)
            if 'DUMPX' in namq:
                print(self.xelend[idex],self.zelend[idex],self.xelest[idex],self.zelest[idex])
                plt.plot([self.xelend[idex],self.xelest[idex]],[self.zelend[idex],self.zelest[idex]],marker='$D$',markersize=13,c='green', transform=rotshift)
        plt.gca().set_aspect('equal', adjustable='box')


    # -----------------------------------------------------------------
    def plotExp(self, rangle, hshift=0, vshift=0):
        '''
        Plots location of the experiment.
        '''
        base = plt.gca()
        rot = transforms.Affine2D().rotate_deg(rangle)+base.transData
        offset = transforms.Affine2D().translate(hshift, vshift)
        rotshift=rot+offset
        xqpos=[]
        zqpos=[]
        for idex,namq in enumerate(self.nameel):
            #print(typq)
            if 'EXP' in namq:
                print(self.xelend[idex],self.zelend[idex],self.xelest[idex],self.zelest[idex])
                plt.plot([self.xelend[idex],self.xelest[idex]],[self.zelend[idex],self.zelest[idex]],marker='$E$',markersize=13,c='green',transform=rotshift)
        plt.gca().set_aspect('equal', adjustable='box')


    # -----------------------------------------------------------------
    def plotSources(self, rangle):
        '''
        Plots location of the ion sources.
        '''
        base = plt.gca()
        rot = transforms.Affine2D().rotate_deg(rangle)+base.transData
        xqpos=[]
        zqpos=[]
        for idex,namq in enumerate(self.nameel):
            #print(typq)
            if 'SOURCES' in namq:
                print(self.xelend[idex],self.zelend[idex],self.xelest[idex],self.zelest[idex])
                plt.plot([self.xelend[idex],self.xelest[idex]],[self.zelend[idex],self.zelest[idex]],marker='$IS$',markersize=14,c='orange',transform=rot)
        plt.gca().set_aspect('equal', adjustable='box')


    # -----------------------------------------------------------------
    def plotECRSources(self, rangle):
        '''
        Plots location of the sources
        '''
        base = plt.gca()
        rot = transforms.Affine2D().rotate_deg(rangle)+base.transData
        xqpos=[]
        zqpos=[]
        for idex,namq in enumerate(self.nameel):
            #print(typq)
            if 'ECR' in namq:
                print(self.xelend[idex],self.zelend[idex],self.xelest[idex],self.zelest[idex])
                mstyle = '$'+namq[1:-1]+'$'
                plt.plot([self.xelend[idex],self.xelest[idex]],[self.zelend[idex],self.zelest[idex]],marker=mstyle,markersize=20,c='orange',transform=rot)
        plt.gca().set_aspect('equal', adjustable='box')


    # -----------------------------------------------------------------
    def printCoordinates(self,elnr=-100):
        '''
        For debugging: print coordinates for an element number elnr.
        If elnr==-100 print coordinates for all elements.
        '''
        print("type, xstart,xmiddle,xend,length")
        print("name, zstart, zmiddle, zend, cleng")
        if elnr==-100:
            for idex,elend in enumerate(self.xelend):
                print('element: ',self.typeel[idex],self.xelest[idex],self.xelect[idex],elend,self.lengel[idex])
                print('         ',self.nameel[idex],self.zelest[idex],self.zelect[idex],self.zelend[idex],self.cleng[idex])
        else:
            print('element: ',self.typeel[elnr],self.xelest[elnr],self.xelect[elnr],self.xelend[elnr],self.lengel[elnr])
            print('         ',self.nameel[elnr],self.zelest[elnr],self.zelect[elnr],self.zelend[elnr],self.cleng[elnr])


    # -----------------------------------------------------------------
    def shiftCoordinates(self, dx=0.0, dy=0.0, dz=0.0):
        '''
        Shift coordinates of all objects.
        '''
        # X:
        for idex,ele in enumerate(self.xelend):
            self.xelend[idex]=ele+dx
        for idex,ele in enumerate(self.xelect):
            self.xelect[idex]=ele+dx
        for idex,ele in enumerate(self.xelest):
            self.xelest[idex]=ele+dx
        # Y:
        for idey,elend in enumerate(self.yelend):
            self.yelend[idey]=elend+dy
        for idey,ele in enumerate(self.yelect):
            self.yelect[idey]=ele+dy
        for idey,elest in enumerate(self.yelest):
            self.yelest[idey]=elest+dy
        #Z:
        for idez,elend in enumerate(self.zelend):
            self.zelend[idez]=elend+dz
        for idez,ele in enumerate(self.zelect):
            self.zelect[idez]=ele+dz
        for idez,elest in enumerate(self.zelest):
            self.zelest[idez]=elest+dz

    # -----------------------------------------------------------------
    def removeElements(self, rinitial=0, rfinal=0):
        '''
        Removes rinitial initial and rfinal final elements of a survey data.
        '''
        rend=len(self.xelest)-rfinal
        #print(rinitial, rfinal)
        # ini, start:
        self.xelest=self.xelest[rinitial:rend]
        self.yelest=self.yelest[rinitial:rend]
        self.zelest=self.zelest[rinitial:rend]
        # ini, center:
        self.xelect=self.xelect[rinitial:rend]
        self.yelect=self.yelect[rinitial:rend]
        self.zelect=self.zelect[rinitial:rend]
        # ini, end:
        self.xelend=self.xelend[rinitial:rend]
        self.yelend=self.yelend[rinitial:rend]
        self.zelend=self.zelend[rinitial:rend]
        # ini, the this:
        self.typeel=self.typeel[rinitial:rend]
        self.nameel=self.nameel[rinitial:rend]

    # -----------------------------------------------------------------
    def plotSurvey(self, rangle=0, hshift=0, vshift=0, projection='XZ', qcolor='red', dsplit=True, kickers=True):
        '''
        Assembly all in single procedure!
        '''
        #self.plotVac(rangle, hshift, vshift)
        self.plotDrift(rangle, hshift, vshift, projection)
        self.plotQuads(rangle, hshift, vshift, projection, color=qcolor)
        self.plotDipoles(rangle, hshift, vshift, projection, dsplit)
        self.plotPatient(rangle, hshift, vshift, projection)
        self.plotRF(rangle, hshift, vshift, projection)
        self.plotDump(rangle, hshift, vshift, projection)
        if kickers:
            self.plotKickers(rangle, hshift, vshift, projection)
        #self.plotSources(rangle)
        #self.plotECRSources(rangle)
        self.plotExp(rangle, hshift, vshift)


