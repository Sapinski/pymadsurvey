
This is a python module which uses matplotlib to plot MADX survey files.

author: Mariusz Sapinski, 
initial commit: February 29, 2020

files:
 - pyMadSurvey.py - the module
 - example.py - an example how to use the module
 - pimms.suv - MADX survey file for PIMMS synchrotron
 - pimms.png - a visualisation of the above survey file
 - make_transparent.py - python script to remove white background in PNG file; usefull if one needs to overlap the MADX layout over a plan of building
